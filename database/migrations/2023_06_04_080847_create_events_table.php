<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->uuid('id');
            $table->foreignUuid('user_id')->constrained('users');
            $table->string('event_name');
            $table->date('event_date');
            $table->time('event_time')->nullable(); //может быть стоит чтобы было опциональным
            $table->foreignUuid('city_id')->nullable()->constrained('cities');
            $table->string('event_place')->nullable();
            $table->string('description')->nullable();
            $table->string('photos')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE events ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
};
