<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('category_name')->unique();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE categories ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('categories_posts', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->foreignUuid('category_id')->constrained('categories');
            $table->foreignUuid('post_id')->constrained('posts');
        });
        DB::statement('ALTER TABLE categories_posts ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('categories_posts');
    }
};
