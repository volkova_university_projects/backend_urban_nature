<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->foreignUuid('user_id')->constrained('users');
            $table->foreignUuid('post_id')->nullable()->constrained('posts')->cascadeOnDelete();
            $table->foreignUuid('thread_id')->nullable()->constrained('threads')->cascadeOnDelete();
//            $table->string('chat_id')->nullable();
//            $table->foreignUuid('chat_id')->nullable()->constrained('chats')->cascadeOnDelete();
            $table->longText('comment_text');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE comments ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
};
