<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reactions', function (Blueprint $table) {
            $table->uuid('id');
            $table->foreignUuid('user_id')->constrained('users');
            $table->foreignUuid('post_id')->nullable()->constrained('posts')->cascadeOnDelete();
            $table->foreignUuid('comment_id')->nullable()->constrained('comments')->cascadeOnDelete();
            $table->string('reaction');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE reactions ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        DB::statement("ALTER TABLE reactions ADD CONSTRAINT like_or_dislike CHECK (reaction = ANY (ARRAY['like'::bpchar, 'dislike'::bpchar]));");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reactions');
    }
};
