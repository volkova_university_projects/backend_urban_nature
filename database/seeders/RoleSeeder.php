<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                ['id' => 'b7ec224c-2ad9-41ea-9771-7a01efbb4cb1', 'role' => 'client'],
                ['id' => '01cf9fdb-12da-4127-bea6-08b4847ac694', 'role' => 'activist'],
                ['id' => '13d29121-c6c1-4a4c-8ae2-83b2270cca73', 'role' => 'admin'],
                ['id' => '69074ede-be6a-4052-8ee8-52f9ebfc7d35', 'role' => 'moderator'],
                ['id' => '1d5f8be4-fa12-436e-bf12-28917f80283a', 'role' => 'banned'],
            ]
        );
    }
}
