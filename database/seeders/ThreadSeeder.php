<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ThreadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('threads')->insert(
            [
                ['id' => 'c0360c87-0cc2-4385-a036-7f1a79cef345', 'user_id' => 'a44901be-015b-47be-84cb-0ede775a6526', 'thread_title' => 'Зеленый мир: советы и рекомендации для экологического образа жизни'],
                ['id' => '62103ae8-1fc8-4bd2-92a6-18bf6cd707c1', 'user_id' => 'f1cf4734-06d0-4be8-97da-d898fe522b47', 'thread_title' => 'Путешествия по зеленым уголкам мира'],
                ['id' => 'dfacc7f3-2277-490e-99a3-e154069d7fe3', 'user_id' => 'af1596f8-c691-4f61-8876-41b6fdb7a07a', 'thread_title' => 'Все об утилизации и переработке отходов'],
                ['id' => '4e82f244-c9ec-403e-b971-96040686a6a9', 'user_id' => '2c8efd7c-96b6-43f7-ae38-6cb2c3d87a38', 'thread_title' => 'Участок для всех: как создать свой сад и сделать его экологически чистым'],
                ['id' => '98b825f5-37e1-4fb8-86a3-fb548d7c4583', 'user_id' => 'ac411421-5440-4e76-b9e0-2c831a8ac22a', 'thread_title' => 'Сохраняем природу: проекты по охране животных и растительности'],
                ['id' => '12b37433-adf2-4f1e-aacd-0f6b206a2260', 'user_id' => 'a4b8d0d2-b24a-4e43-b0d3-6ccd05032bd4', 'thread_title' => 'Экологические книги: обзоры и рекомендации'],
                ['id' => 'af58c6c4-f155-44f7-b8f5-82f017196046', 'user_id' => '6b73204d-8b91-459b-983d-61fa849998a5', 'thread_title' => 'Экологические эксперименты: что можно сделать самостоятельно дома'],
                ['id' => '523ddaec-31df-4abb-bfe4-7e4579762c54', 'user_id' => '404ccf25-a365-438c-b7a0-ca5e2d57e43b', 'thread_title' => 'Экологические прогулки: как наслаждаться природой и не наносить ей вреда'],
                ['id' => '201a5cd6-0dec-44ae-995a-21e9fecd7837', 'user_id' => 'f4d2d3b8-fd38-4c50-a722-67bb85e5a68a', 'thread_title' => 'Экологические гаджеты: как выбрать технику с наименьшим негативным воздействием на окружающую среду'],
                ['id' => '118588ae-ba9b-4994-bc97-3054c26551ae', 'user_id' => '48eed20c-4d6b-4bda-b899-d98c9af010c4', 'thread_title' => 'Как люди изменяют мир к лучшему'],
                ['id' => 'ab690aae-e735-4950-8e6e-42364c16ec2a', 'user_id' => '794ca20a-9471-46d7-ad23-21765cbd4eb8', 'thread_title' => 'Экологические решения в бизнесе: как компании внедряют экологические практики'],
                ['id' => '5c96850e-72ae-47f0-af45-b35bc213a5bb', 'user_id' => '4a4a95f2-b312-4c70-9fb8-21dab0defbbd', 'thread_title' => 'Эко-мода: как быть стильной и экологичной'],
                ['id' => 'c4c0e1f9-f720-4c29-b02a-16a3cf9e77df', 'user_id' => '3937cc8b-156f-4e3d-94cd-e1a6fbd12747', 'thread_title' => 'Экологические лайфхаки: как упростить свою жизнь и не нанести вреда окружающей среде'],
                ['id' => '64db0bee-93dc-458d-857b-50d8c61f220a', 'user_id' => 'b951a693-367f-46c6-bb89-cb219769e89d', 'thread_title' => 'Экологический туризм: как отдыхать и не вредить природе'],
                ['id' => '984bb3f6-789e-41da-94e2-489a18e7dd24', 'user_id' => '37adca82-697b-4a88-af05-1cb8f68b8a5c', 'thread_title' => 'Как принимать участие в экологических инициативах и движениях'],
                ['id' => 'd929f373-5f58-4bd0-b22d-f83102245565', 'user_id' => 'a52f108b-122e-400a-9531-24d7d625bc4f', 'thread_title' => 'Как бороться с загрязнением воды и сохранять рыбные ресурсы'],
                ['id' => '5d87f02e-6a94-49e1-90e6-748dcbb139fc', 'user_id' => '3e72f9e6-15fe-455d-b4bb-12408fea1d63', 'thread_title' => 'Как поддерживать молодежные инициативы'],
                ['id' => '379c765b-d7fc-4851-a2e7-9fa24bceb8ea', 'user_id' => '586f87a5-9ff2-4025-af00-9d711f1e1818', 'thread_title' => 'Как поддерживать новые идеи в сфере экологии'],
            ]
        );
    }
}
