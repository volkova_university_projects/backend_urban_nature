<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                ['id' => 'a45b8a43-c81b-49a5-8d90-d37339b22b18', 'category_name' => 'Общее'],
                ['id' => '142bb285-74b0-4b39-944b-5a76ac246dac', 'category_name' => 'Длиннопост'],
                ['id' => 'c8308256-9074-4f0a-9026-94e04702d330', 'category_name' => 'Разговоры о жизни'],
                ['id' => 'c1319537-5ca9-4d31-a544-20991b85b3a4', 'category_name' => 'Экология'],
                ['id' => 'fd108a56-6186-4917-8094-271928f79b1e', 'category_name' => 'Наука'],
                ['id' => '67649294-fb0f-4b72-bbfb-11dab2e65a57', 'category_name' => 'Факты'],
                ['id' => '506938ec-e890-44b6-b1c3-724dc476eedd', 'category_name' => 'Лайфхаки'],
                ['id' => 'f0180c6a-4f13-4263-bf39-4e9cc64f3a41', 'category_name' => 'Политика'],
                ['id' => 'dc90ef26-f1c8-4526-b81e-311f2aa2ac9a', 'category_name' => 'Личная история'],
                ['id' => '331caea6-26b3-4ddd-9791-1c23fafdc3ff', 'category_name' => 'Правила'],
                ['id' => 'e0a58e13-a19a-4718-808d-3c78464ca995', 'category_name' => 'Новости'],
                ['id' => 'ff4c70a8-2d56-43aa-bc74-f6045421c728', 'category_name' => 'События'],
                ['id' => 'cecf67df-af92-4269-adcb-55ff73cd6482', 'category_name' => 'Советы'],
                ['id' => '11856f3e-67c8-4fa6-a64e-eb71fc7011c5', 'category_name' => 'Эко-мода'],
                ['id' => 'e9b5391c-9727-4e1c-81fc-e07f9f7ce526', 'category_name' => 'Экологические проблемы'],
                ['id' => 'da9417da-a0b4-46bd-9b38-e92f5d3ae660', 'category_name' => 'Экологические катастрофы'],
                ['id' => 'c567df78-788a-4bb8-9573-a8654253fcd8', 'category_name' => 'Переработка отходов'],
                ['id' => '8fdf6c7c-a19d-4bd6-ab72-a3e3a7b0e865', 'category_name' => 'Путешествия'],
                ['id' => 'f828bc81-ea2c-49c4-9728-a0c1f5166e10', 'category_name' => 'Развлечения'],
                ['id' => 'c6787946-b525-4663-a688-6680a3b0d483', 'category_name' => 'Куда сходить'],
                ['id' => '40214a1d-69ac-4daa-a21c-27ce793bf4e3', 'category_name' => 'Мой мир'],
                ['id' => '1dc4588f-0249-46f4-a069-ce908da9fe60', 'category_name' => 'Чувства'],
                ['id' => '28dd4ef5-c814-49d3-b13c-26d404e01685', 'category_name' => 'Эмоции'],
                ['id' => '28ca1c70-77f2-4074-9308-27d81c6842f9', 'category_name' => 'Настроение'],
                ['id' => '05dd12fd-a015-4e2d-a13a-443b52b29c20', 'category_name' => 'Статистика'],
                ['id' => '2006988c-af47-47b1-9c76-5107ca90e4d9', 'category_name' => 'Цифры'],
                ['id' => 'b488b825-865a-457d-bcc0-cc9391a32f7c', 'category_name' => 'Рекомендации'],
                ['id' => '4c783fdb-dda7-45ab-8cae-84760bef2fcb', 'category_name' => 'Книги'],
                ['id' => 'abbf1fa8-f7ad-400f-8f14-4c2b4d0198a8', 'category_name' => 'Журналы'],
                ['id' => '87f42fca-5414-422f-b55e-02d5ce985469', 'category_name' => 'Технологии'],
                ['id' => '6e2fee6f-c77d-4445-8ceb-09e9939f558d', 'category_name' => 'Фильмы'],
                ['id' => '5a1ca7ba-d8dd-4965-bc23-d3320d8fafdb', 'category_name' => 'Эксперименты'],
                ['id' => '04a2709b-8c2a-4c77-ac5e-ae7e6b37d230', 'category_name' => 'Курсы'],
                ['id' => 'f4d12b4f-721c-435a-9f43-bad79d238d68', 'category_name' => 'Семинары'],
                ['id' => 'd8fed750-64b2-4911-ad54-067071eb02bd', 'category_name' => 'Онлайн'],
                ['id' => 'e5e44cab-4517-4ebf-b154-42f1e717a645', 'category_name' => 'Офлайн'],
                ['id' => 'cbf31be2-fc69-47f9-b369-41118011ef37', 'category_name' => 'Прогулки'],
                ['id' => 'a8b22669-1ff4-4d4f-a785-743ca9f3ac14', 'category_name' => 'Праздники'],
                ['id' => '9553e341-5d11-420f-b7d6-0e6a96bf62a0', 'category_name' => 'Необычное'],
                ['id' => '960fac82-1ab4-4a08-9d9d-61649e3c2690', 'category_name' => 'Проекты'],
                ['id' => '4b4f2b93-b0a8-4f90-a88a-4fb9775a84cc', 'category_name' => 'Стартапы'],
                ['id' => '84ea24a0-b071-4932-9d74-b68f703c10c4', 'category_name' => 'Для всех'],
        ]);
    }
}
