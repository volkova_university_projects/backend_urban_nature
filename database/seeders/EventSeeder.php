<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        protected $fillable = ['user_id', 'event_name',
//        'event_date', 'event_time', 'city_id',
//        'event_place', 'description', 'photos'];
        DB::table('events')->insert(
            [
                ['id' => 'f69da712-2b77-4445-a18a-cf73b53a6d60', 'user_id' => 'a44901be-015b-47be-84cb-0ede775a6526', 'event_name' => 'Голосование за установку урн для разделения мусора', 'event_date' => '2023-01-01', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => 'c36959f8-a12a-4d3a-9005-c7fec6783d48', 'user_id' => 'f1cf4734-06d0-4be8-97da-d898fe522b47', 'event_name' => 'Семинар экологов университета онлайн', 'event_date' => '2023-01-02', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => 'c70d85a3-d614-49b6-99b6-c90dc61750a1', 'user_id' => 'af1596f8-c691-4f61-8876-41b6fdb7a07a', 'event_name' => 'Семинар по правилам поведения онлайн', 'event_date' => '2023-01-03', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '3848644a-177a-4b17-bd9e-13675cd55df6', 'user_id' => '2c8efd7c-96b6-43f7-ae38-6cb2c3d87a38', 'event_name' => 'Семинар по теме Гигиена воздуха онлайн', 'event_date' => '2023-01-04', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '9aca8848-9ce2-4b54-a781-d2a906c6bfa9', 'user_id' => 'ac411421-5440-4e76-b9e0-2c831a8ac22a', 'event_name' => 'Семинар по теме Опасность выбросов онлайн', 'event_date' => '2023-02-03', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '0607e414-d733-4dd2-ad2f-28ee2f0e438f', 'user_id' => 'a4b8d0d2-b24a-4e43-b0d3-6ccd05032bd4', 'event_name' => 'Семинар по теме Культура экологии онлайн', 'event_date' => '2023-02-10', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => 'd05c4cd2-16f9-45f5-a1bb-9d249be2e80e', 'user_id' => '6b73204d-8b91-459b-983d-61fa849998a5', 'event_name' => 'Семинар по теме Разговоры о жизни онлайн', 'event_date' => '2023-03-01', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '84c537f9-e610-4573-9642-4029a30e0e58', 'user_id' => '404ccf25-a365-438c-b7a0-ca5e2d57e43b', 'event_name' => 'Голосование за проведение мероприятия на площади Пушкина', 'event_date' => '2023-03-03', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => 'bf4ef03a-c4ff-41bc-9bb3-f71ff76f6612', 'user_id' => 'f4d2d3b8-fd38-4c50-a722-67bb85e5a68a', 'event_name' => 'Встреча на площади Пушкина', 'event_date' => '2023-05-03', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '01613903-9e46-403c-9c67-3e5582607b63', 'user_id' => '48eed20c-4d6b-4bda-b899-d98c9af010c4', 'event_name' => 'Встреча на площади Ленина', 'event_date' => '2023-02-20', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '97071285-7ffe-491d-ac4b-6fae31577866', 'user_id' => '794ca20a-9471-46d7-ad23-21765cbd4eb8', 'event_name' => 'Праздник единства любителй природы', 'event_date' => '2023-04-11', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => 'aaae5e23-6144-4d6e-8bdf-a15c389f6e1a', 'user_id' => '4a4a95f2-b312-4c70-9fb8-21dab0defbbd', 'event_name' => 'Семинар по теме Опасность отходов онлайн', 'event_date' => '2023-01-02', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => 'aeb3c777-de37-481d-9a18-923f67385d1c', 'user_id' => '3937cc8b-156f-4e3d-94cd-e1a6fbd12747', 'event_name' => 'Семинар по теме Культура экологичного быта онлайн', 'event_date' => '2023-01-02', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '11445245-b37b-48af-967c-0d824a817ed4', 'user_id' => 'b951a693-367f-46c6-bb89-cb219769e89d', 'event_name' => 'Семинар по теме Экологичное отношение к жизни онлайн', 'event_date' => '2023-03-11', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => 'c419e17c-e8be-4247-9415-9599d8865bc9', 'user_id' => '37adca82-697b-4a88-af05-1cb8f68b8a5c', 'event_name' => 'Семинар по теме Экологичное отношение к окружению онлайн', 'event_date' => '2023-01-02', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => 'b3bf58b7-579b-4551-b7df-18fad1bcce28', 'user_id' => 'a52f108b-122e-400a-9531-24d7d625bc4f', 'event_name' => 'Семинар по теме Разговоры об экологии онлайн', 'event_date' => '2023-05-21', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '17c0039e-be34-4450-bee6-ea068754c6ad', 'user_id' => '3e72f9e6-15fe-455d-b4bb-12408fea1d63', 'event_name' => 'Семинар по теме Разговоры о поведении людей онлайн', 'event_date' => '2023-05-12', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '01cd7be2-297b-4b57-a9f2-4a5a766ea15c', 'user_id' => '586f87a5-9ff2-4025-af00-9d711f1e1818', 'event_name' => 'Семинар по теме Психология воспитания онлайн', 'event_date' => '2023-01-04', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
                ['id' => '03967271-d357-4c13-a72f-28eab44f5190', 'user_id' => '9b145739-8131-43bd-a527-ddd546eee05e', 'event_name' => 'Семинар по теме Психология в области биологии онлайн', 'event_date' => '2023-01-07', 'event_time' => null, 'city_id' => null, 'event_place' => null, 'description' => null, 'photos' => null],
            ]
        );
    }
}
