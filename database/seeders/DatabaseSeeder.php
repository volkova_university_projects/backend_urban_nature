<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CitySeeder::class,
            UserSeeder::class,
            RoleSeeder::class,
            BlogSeeder::class,
            PostSeeder::class,
            CategorySeeder::class,
            EventSeeder::class,
            ThreadSeeder::class,
            CommentSeeder::class,
            ReactionSeeder::class,
            SubscriptionSeeder::class,
            CategoryPostSeeder::class,
            RoleUserSeeder::class,
        ]);
    }
}
