<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Thread extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'threads';

    protected $fillable = ['user_id', 'thread_title'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function withFilters($filter)
    {
        return Thread::query()
            ->where('thread_title', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('user', 'first_name', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('user', 'last_name', 'ilike', ('%' . $filter . '%'))
            ->get();
    }

//    public function comment(): BelongsTo
//    {
//        return $this->belongsTo(Comment::class, 'comments_threads');
//    }
}
