<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, HasUuids;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'birth_date',
        'city',
        'phone',
        'email',
        'password',
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $primaryKey = 'id';
    protected $keyType = 'string';


    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public function cities(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'roles'=> $this->roles->keyBy('role')->keys(),
            'name' => array_values($this->only('full_name'))
        ];
    }

    public static function withRole($user_id, $role_id) {
        return User::query()
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'user_roles.role_id', '=', 'roles.id')
            ->where('user_roles.user_id', '=', $user_id)
            ->where('user_roles.role_id', '=', $role_id);
    }

    public static function usersWithFilter($filter)
    {
        return User::query()
            ->where('users.first_name', 'ilike', ($filter . '%'))
            ->orWhere('users.last_name', 'ilike', ($filter . '%'))
            ->orWhere('users.phone', 'ilike', ('%' . $filter . '%'))
            ->orWhere('users.email', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('cities', 'city_name', 'ilike', $filter)
            ->orWhereRelation('roles', 'role', 'ilike', $filter)
            ->get();
    }



}
