<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Comment extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'comments';

    protected $fillable = ['user_id', 'post_id', 'thread_id', 'comment_text'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function blog(): BelongsTo
    {
        return $this->belongsTo(Blog::class);
    }

    public function thread(): BelongsTo
    {
        return $this->belongsTo(Thread::class, 'thread_id');
    }

    public static function withFilters($filter)
    {
        return Comment::query()
            ->where('comments.comment_text', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('user', 'first_name', 'ilike', $filter)
            ->orWhereRelation('user', 'last_name', 'ilike', $filter)
            ->get();
    }

    public static function byPost($id)
    {
        return Comment::query()
            ->where('post_id', '=', $id)
            ->where('created_at', '>=', Carbon::now()->subMonths(12))
            ->get()->count();
    }

    public static function byThread($id)
    {
        return Comment::query()
            ->where('thread_id', '=', $id)
            ->where('created_at', '>=', Carbon::now()->subMonths(12))
            ->get()->count();
    }

    public static function byUser($id)
    {
        return Comment::query()
            ->where('user_id', '=', $id)
            ->where('created_at', '>=', Carbon::now()->subMonths(6))
            ->get();
    }

//    public static function countItems($post_id)
//    {
//        return Comment::query()
//            ->
//    }

}
