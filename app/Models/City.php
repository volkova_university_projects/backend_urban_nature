<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class City extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'cities';

    protected $fillable = ['city_name', 'country'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function user(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users');
    }

    public static function citiesWithFilter($filter)
    {
        return City::query()
            ->where('cities.city_name', 'ilike', ('%' . $filter . '%'))
            ->orWhere('cities.country', 'ilike', $filter)
            ->get();
    }
}
