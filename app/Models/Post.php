<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Post extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'posts';

    protected $fillable = ['post_title', 'blog_id', 'category_id', 'post_text', 'photos'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function blog(): BelongsTo
    {
        return $this->belongsTo(Blog::class);
    }

    public function category(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'categories_posts');
    }

    public static function withFilters($filter)
    {
        return Post::query()
            ->where('post_title', 'ilike', ('%' . $filter . '%'))
//            ->orWhere('post_text', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('blog', 'blog_title', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('category', 'category_name', 'ilike', $filter)
            ->get();
    }


    public static function byBlog(string $id)
    {
        return Post::query()
            ->where('blog_id', $id)
            ->get();
    }

    public static function byBlogWithFilter(string $id, $filter)
    {
        return Post::query()
            ->where('blog_id', $id)
            ->where('post_title', 'ilike', ('%' . $filter . '%'))
            ->orWhere('post_text', 'ilike', ('%' . $filter . '%'))
            ->get();
    }

    public static function countItems($blog_id)
    {
        return Post::query()
            ->where('blog_id', '=', $blog_id)
            ->where('created_at', '>=', Carbon::now()->subMonths(24))
            ->get()->count();
    }

}
