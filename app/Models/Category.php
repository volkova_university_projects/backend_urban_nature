<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'categories';

    protected $fillable = ['category_name'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function post(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'categories_posts');
    }

    public static function withPost(string $id)
    {
        return Category::query()
            ->whereRelation('post', 'posts.id', '=', $id)
            ->select(
                'categories.id as category_id',
                'categories.category_name as category_name'
            )
            ->get();
    }

    public static function withFilters($filter)
    {
        return Category::query()
            ->where('category_name', 'ilike', ('%' . $filter .'%'))
            ->get();
    }


}
