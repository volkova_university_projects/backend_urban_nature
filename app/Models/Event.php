<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Event extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'events';

    protected $fillable = ['user_id', 'event_name',
        'event_date', 'event_time', 'city_id',
        'event_place', 'description', 'photos'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cities(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id');
    }


    public static function withFilter($filter)
    {
        return Event::query()
            ->where('event_name', 'ilike', ('%' . $filter . '%'))
            ->orWhere('event_date', 'ilike', $filter)
            ->orWhere('city_id', '=', $filter)
            ->orWhere('event_place', 'ilike', ('%' . $filter . '%'))
            ->get();
    }
}
