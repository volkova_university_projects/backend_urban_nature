<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Subscription extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'subscriptions';

    protected $fillable = ['user_id', 'blog_id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function blog(): BelongsTo
    {
        return $this->belongsTo(Blog::class);
    }

    public static function withUser(string $id)
    {
        return Subscription::query()
            ->where('user_id', '=', $id)
            ->where('created_at', '>=', Carbon::now()->subMonths(12))
            ->get();
    }

    public static function countItems(string $id) {
        return Subscription::query()
            ->where('blog_id', '=', $id)
            ->where('created_at', '>=', Carbon::now()->subMonths(36))
            ->get()->count();
    }

    public static function withUserAndFilters($filter, string $id)
    {
        return Subscription::query()
            ->where('user_id', '=', $id)
            ->whereRelation('blog', 'blog_title', 'ilike', ('%' . $filter . '%'))
            ->get();
    }

    public static function inTable($user_id, $blog_id)
    {
        return Subscription::query()
            ->where('user_id', 'ilike', $user_id)
            ->where('blog_id', 'ilike', $blog_id)
            ->first();
    }
}
