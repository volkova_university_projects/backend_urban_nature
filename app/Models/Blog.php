<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Blog extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'blogs';

    protected $fillable = ['user_id', 'blog_title', 'blog_description', 'blog_avatar'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function post(): HasMany
    {
        return $this->hasMany(Post::class);
    }

    public static function withFilters($filter)
    {
        return Blog::query()
//            ->join('post', 'posts.post_id', '=', '')
            ->where('blogs.blog_title', 'ilike', ('%' . $filter . '%'))
            ->get();
    }

    public static function withName(string $id) {
        return Blog::query()
            ->where('id', $id)
            ->get();
    }

    public static function isOwner($user_id, $blog_id)
    {
        return Blog::query()
            ->where('id', '=', $blog_id)
            ->where('user_id', '=', $user_id)
            ->first();
    }

    public static function owner($user_id)
    {
        return Blog::query()
            ->where('user_id', '=', $user_id)
            ->where('created_at', '>=', Carbon::now()->subMonths(18))
            ->get();
    }

    public static function ownerWithFilters($user_id, $filter)
    {
        return Blog::query()
            ->where('user_id', '=', $user_id)
            ->where('blogs.blog_title', 'ilike', ('%' . $filter . '%'))
            ->get();
    }
}
