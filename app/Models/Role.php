<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'roles';

    protected $fillable = ['role'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function user(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_roles');
    }

    public static function withAttributes(): Collection|array
    {
        return Role::query()
            ->select(
                'roles.id as role_id',
                'roles.role as role_name'
            )->get();
    }



    public static function client() {
        return Role::query()->where('roles.role', '=', 'client')->first();
    }

    public static function activist() {
        return Role::query()->where('roles.role', '=', 'activist')->first();
    }

    public static function admin() {
        return Role::query()->where('roles.role', '=', 'admin')->first();
    }

    public static function blogger() {
        return Role::query()->where('roles.role', '=', 'blogger')->first();
    }

    public static function banned() {
        return Role::query()->where('roles.role', '=', 'banned')->first();
    }

    public static function moderator() {
        return Role::query()->where('roles.role', '=', 'moderator')->first();
    }

}
