<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\DB;

class Reaction extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'reactions';

    protected $fillable = ['user_id', 'post_id', 'comment_id', 'reaction'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    public function comment(): BelongsTo
    {
        return $this->belongsTo(Comment::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function withFilter($filter)
    {
        return Reaction::query()
            ->whereRelation('user', 'first_name', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('user', 'last_name', 'ilike', ('%' . $filter . '%'))
            ->get();
    }

    public static function withReaction($reaction)
    {
        return Reaction::query()
            ->where('reaction', 'ilike', $reaction)
            ->get();
    }

    public static function withFilterAndReaction($filter, $reaction)
    {
        return Reaction::query()
            ->where('reaction', 'ilike', $reaction)
            ->whereRelation('user', 'first_name', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('user', 'last_name', 'ilike', ('%' . $filter . '%'))
            ->get();
    }


    public static function likesComment(string $id)
    {
        return Reaction::query()
            ->where('reaction', '=', 'like')
            ->where('comment_id', '=', $id)
            ->get();
    }

    public static function dislikesComment(string $id)
    {
        return Reaction::query()
            ->where('reaction', '=', 'dislike')
            ->where('comment_id', '=', $id)
            ->get();
    }

    public static function likesPost(string $id)
    {
        return Reaction::query()
            ->where('reaction', '=', 'like')
            ->where('post_id', '=', $id)
            ->get();
    }

    public static function dislikesPost(string $id)
    {
        return Reaction::query()
            ->where('reaction', '=', 'dislike')
            ->where('post_id', '=', $id)
            ->get();
    }


    public static function userLikes(string $id)
    {
        return Reaction::query()
            ->where('reaction', '=', 'like')
            ->where('user_id', '=', $id)
            ->where('created_at', '>=', Carbon::now()->subMonths(3))
            ->get();
    }

    public static function userDislikes(string $id)
    {
        return Reaction::query()
            ->where('reaction', '=', 'dislike')
            ->where('user_id', '=', $id)
            ->where('created_at', '>=', Carbon::now()->subMonths(3))
            ->get();
    }


    public static function blogLikes(string $id)
    {
        return Reaction::query()
            ->join('posts', 'posts.id', '=', 'reactions.post_id')
            ->join('blogs', 'blogs.id', '=', 'posts.blog_id')
            ->where('blogs.id', '=', $id)
            ->where('reaction', '=', 'like')
            ->where('created_at', '>=', Carbon::now()->subMonths(4))
            ->get();
    }

    public static function blogDislikes(string $id)
    {
        return Reaction::query()
            ->join('posts', 'posts.id', '=', 'reactions.post_id')
            ->join('blogs', 'blogs.id', '=', 'posts.blog_id')
            ->where('blogs.id', '=', $id)
            ->where('reaction', '=', 'dislike')
            ->where('created_at', '>=', Carbon::now()->subMonths(4))
            ->get();
    }

    public static function withBlog(string $id)
    {
        return Reaction::query()
            ->join('posts', 'posts.id', '=', 'reactions.post_id')
            ->join('blogs', 'blogs.id', '=', 'posts.blog_id')
            ->where('blogs.id', '=', $id)
            ->get();
    }



    public static function commentLikes(string $id): Collection|array
    {
        return Reaction::query()
            ->where('comment_id', '=', $id)
            ->where('reaction', '=', 'like')
            ->where('created_at', '>=', Carbon::now()->subMonths(5))
            ->get();
    }

    public static function commentDislikes(string $id): Collection|array
    {
        return Reaction::query()
            ->where('comment_id', '=', $id)
            ->where('reaction', '=', 'dislike')
            ->where('created_at', '>=', Carbon::now()->subMonths(5))
            ->get();
    }

    public static function postLikes(string $id)
    {
        return Reaction::query()
            ->where('post_id', '=', $id)
            ->where('reaction', '=', 'like')
            ->where('created_at', '>=', Carbon::now()->subMonths(6))
            ->get();
    }

    public static function postDislikes(string $id)
    {
        return Reaction::query()
            ->where('post_id', '=', $id)
            ->where('reaction', '=', 'dislike')
            ->where('created_at', '>=', Carbon::now()->subMonths(6))
            ->get();
    }


    public static function threadLikes(string $id)
    {
        return Reaction::query()
            ->whereRelation('comment', 'thread_id', '=', $id)
            ->where('reaction', '=', 'like')
            ->where('created_at', '>=', Carbon::now()->subMonths(5))
            ->get();
    }

    public static function threadDislikes(string $id)
    {
        return Reaction::query()
            ->whereRelation('comment', 'thread_id', '=', $id)
            ->where('reaction', '=', 'dislike')
            ->where('created_at', '>=', Carbon::now()->subMonths(5))
            ->get();
    }

}
