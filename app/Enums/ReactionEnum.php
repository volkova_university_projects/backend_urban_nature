<?php
namespace App\Enums;

enum ReactionEnum:string {
    case Like = 'like';
    case Dislike = 'dislike';
}
