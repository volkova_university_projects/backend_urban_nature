<?php
namespace App\Enums;

enum RoleEnum:string {
    case Client = 'client'; // just simple user, who can only read, leave comment, reacting on posts by buttons
    case Activist = 'activist'; // superior user, who can organize an event and hand out special points
    case Admin = 'admin'; // superuser, who can make all CRUD's
    case Moderator = 'moderator'; // he is a superuser too, but can't make all CRUD's
    case Banned = 'banned'; // bad user, who did bad things in system
}
