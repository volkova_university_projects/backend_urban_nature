<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
//        $categories = [];
//        foreach ($this->category as $category) {
//            $categories[] = $category->
//        }

        return [
            'post_id' => $this->id,
            'blog_id' => $this->blog_id,
            'blog_title' => $this->blog->blog_title,
            'user_first_name' => $this->blog->user->first_name,
            'user_last_name' => $this->blog->user->last_name,
            'post_title' => $this->post_title,
            'post_text' => $this->post_text,
            'categories' => Category::withPost($this->id),
//            'categories' => $this->category()->id,
            'photos' => $this->photos,
            'created' => $this->created_at,
            'edited' => $this->updated_at

        ];
    }
}
