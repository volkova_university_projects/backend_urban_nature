<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ThreadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'thread_id' => $this->id,
            'user_id' => $this->user->id,
            'user_first_name' => $this->user->first_name,
            'user_last_name' => $this->user->last_name,
            'thread_title' => $this->thread_title,
            'created' => $this->created_at,
            'edited' => $this->updated_at
        ];
    }
}
