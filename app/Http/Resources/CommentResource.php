<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'comment_id' => $this->id,
            'user_id' => $this->user_id,
            'user_first_name' => $this->user->first_name,
            'user_last_name' => $this->user->last_name,
            'post_id' => $this->post_id,
            'chat_id' => $this->chat_id,
            'comment_text' => $this->comment_text,
            'photos' => $this->photos,
            'created' => $this->created_at,
            'edited' => $this->updated_at
        ];
    }
}
