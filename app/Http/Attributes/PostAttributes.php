<?php

namespace App\Http\Attributes;

class PostAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'blog_id' => $request->blog_id,
            'post_title' => $request->post_title,
            'post_text' => $request->post_text,
            'photos' => $request->photos,
        ];
    }
}
