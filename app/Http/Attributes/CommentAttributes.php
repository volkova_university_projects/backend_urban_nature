<?php

namespace App\Http\Attributes;

class CommentAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'user_id' => $request->user_id,
            'post_id' => $request->post_id,
            'chat_id' => $request->chat_id,
            'comment_text' => $request->comment_text,
        ];
    }
}
