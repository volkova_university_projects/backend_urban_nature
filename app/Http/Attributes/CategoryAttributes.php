<?php

namespace App\Http\Attributes;

use Illuminate\Support\Facades\Hash;

class CategoryAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'category_name' => $request->category_name
        ];
    }
}
