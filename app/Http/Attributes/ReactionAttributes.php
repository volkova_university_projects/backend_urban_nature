<?php

namespace App\Http\Attributes;

class ReactionAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'user_id' => $request->user_id,
            'post_id' => $request->post_id,
            'comment_id' => $request->comment_id,
            'reaction' => $request->reaction,
        ];
    }
}
