<?php

namespace App\Http\Attributes;

use Illuminate\Support\Facades\Hash;

class ThreadAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'user_id' => $request->user_id,
            'thread_title' => $request->thread_title,
        ];
    }
}
