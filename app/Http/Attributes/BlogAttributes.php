<?php

namespace App\Http\Attributes;

class BlogAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'user_id' => $request->user_id,
            'blog_title' => $request->blog_title,
            'blog_description' => $request->blog_description,
            'blog_avatar' => $request->blog_avatar,
        ];
    }
}
