<?php

namespace App\Http\Attributes;

use Illuminate\Support\Facades\Hash;

class CityAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'city_name' => $request->city_name,
            'country' => $request->country,
        ];
    }
}
