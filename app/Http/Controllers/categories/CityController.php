<?php

namespace App\Http\Controllers\categories;

use App\Http\Attributes\CityAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCityRequest;
use App\Http\Requests\UpdateCityRequest;
use App\Http\Resources\CityResource;
use App\Models\City;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {

        if (empty(request()->filter)) {
            $query = City::all();
        } else {
            $query = City::citiesWithFilter(request()->filter);
        }
        $users = CityResource::collection($query);
        return (new PaginationService())->pagination($users, 'cities');
    }


//
//    /**
//     * Display the specified resource.
//     *
//     * @param string $id
//     * @return JsonResponse
//     */
//    public function show(string $id): JsonResponse
//    {
//        $city = City::query()->where('id', $id)->first();
//        if (empty($city)) {
//            return response()->json([
//                'status' => false,
//                'massage' => 'This place doesn\'t exist'
//            ], 404);
//        } else {
//            return response()->json([
//                $city
//            ], 200);
//        }
//    }



    /**
     * Register in the system
     *
     * @param CreateCityRequest $request
     * @return JsonResponse
     */
    public function create(CreateCityRequest $request): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        $city_in_table = City::query()
            ->where('city_name', 'ilike', $data->city_name)
            ->first();

        if (!empty($city_in_table)) {
            return response()->json([
                'status' => false,
                'message' => 'City -> ' . $data->city_name . ' -> already exists'
            ], 400);
        }

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            $new_city = new City(
                (new CityAttributes())->attributes($data)
            );
            $new_city->save();

            $created_city = City::query()
                ->where('id', $new_city->id)
                ->first();


            return response()->json([
                'response:' =>
                    ['status' => true,
                        'message' => 'City was successfully created'],
                'created_city:' =>
                    new CityResource($created_city)
            ]);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCityRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateCityRequest $request, string $id): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        $current_city = City::query()
            ->where('id', $id)->first();

        if (empty($current_city)) {
            return response()->json([
                'status' => false,
                'massage' => 'This place doesn\'t exist'
            ], 404);
        }

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->city_name)) {
                if ($data->city_name != $current_city->city_name
                        and City::query()
                                ->where('city_name', $data->city_name)->exists()) {
                    return response()->json([
                        'status' => false,
                        'message' => 'This place already exist'
                    ], 409);
                } else {
                    $current_city->city_name = $data->city_name;
                }
            }
            if (!empty($data->country)) {
                $current_city->country = $data->country;
            }

            if ($current_city->isDirty()) {
                $current_city->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'City was successfully updated'],
                    'updated_city:' =>
                        new CityResource($current_city)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        if (empty(City::query()->find($id)->id)) {
            return response()->json([
                'status' => false,
                'message' => 'This place doesn\'t exist'
            ], 404);
        } else {
            City::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'City was successfully deleted'
            ], 200);
        }
    }
}
