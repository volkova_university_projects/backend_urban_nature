<?php

namespace App\Http\Controllers\blogs;

use App\Http\Attributes\ThreadAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateThreadRequest;
use App\Http\Requests\UpdateThreadRequest;
use App\Http\Resources\ThreadResource;
use App\Models\Thread;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ThreadController extends Controller
{
    //    Route::get('/threads', 'show_all');
    //    Route::post('/threads', 'create');
    //    Route::put('/threads/{id}', 'update');
    //    Route::delete('/threads/{id}', 'delete');

    /**
     * Get all threads.
     *
     * @return JsonResponse
     */
    public function show_all()
    {
        if (empty(request()->filter)) {
            $query = Thread::all();
        } else {
            $query = Thread::withFilters(request()->filter);
        }
        $threads = ThreadResource::collection($query);
        return (new PaginationService())->pagination($threads, 'threads');
    }

    /**
     * Create a new thread.
     *
     * @param CreateThreadRequest $request
     * @return JsonResponse
     */
    public function create(CreateThreadRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $thread_in_table = Thread::query()
            ->where('user_id', 'ilike', $data->user_id)
            ->where('thread_title', 'ilike', $data->thread_title)
            ->first();

        if (!empty($thread_in_table)) {
            return response()->json([
                'status' => false,
                'message' => 'You already have thread -> ' . $data->thread_title . '! Try another!'
            ], 409);
        }


        $new_thread = new Thread(
            (new ThreadAttributes)->attributes($data));
        $new_thread->save();

        return response()->json([
            'response:' =>
                ['status' => true,
                    'message' => 'Thread was successfully created'],
            'created_thread:' =>
                new ThreadResource($new_thread)
        ], 201);
    }


    /**
     * Edit a thread by id.
     *
     * @param UpdateThreadRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateThreadRequest $request, string $id): JsonResponse
    {
        $current_thread = Thread::query()
            ->where('id', $id)->first();

        if (empty($current_thread)) {
            return response()->json([
                'status' => false,
                'message' => 'Thread not found'
            ], 404);
        }

        $thread = request()->only(
            "user_id",
            "thread_title"
        );

        $validator = Validator::make($thread, [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        foreach (array_keys($thread) as $key)
        {
            if (!empty($thread[$key]) && $current_thread->$key != $thread[$key]) {
                $current_thread->$key = $thread[$key];
            }
        }

        if ($current_thread->isDirty()) {
            $current_thread->save();
            return response()->json([
                'response:' =>
                    ['status' => true,
                        'message' => 'Thread was successfully updated'],
                'updated_thread:' =>
                    new ThreadResource($current_thread)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'No changes'
            ]);
        }
    }


    /**
     * Delete a thread by id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_thread  = Thread::query()->find($id);
        if ($current_thread !== null) {
            Thread::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Thread was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Thread doesn\'t exist'
            ], 404);
        }
    }


}
