<?php

namespace App\Http\Controllers\blogs;

use App\Http\Attributes\SubscriptionAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSubscriptionRequest;
use App\Http\Resources\SubscriptionResource;
use App\Models\Blog;
use App\Models\Subscription;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubscriptionController extends Controller
{
    //    Route::get('/subscriptions/{user_id}', 'show_sub_blogs');
    //    Route::post('/subscriptions', 'create');
    //    Route::delete('/subscriptions/{id}', 'delete');

    /**
     * Get all own subscriptions for user by user_id.
     *
     * @return JsonResponse
     */
    public function show_sub_blogs(string $user_id)
    {
        error_log($user_id);
        if (empty(request()->filter)) {
            $query = Subscription::withUser($user_id);
        }
        else {
            $query = Subscription::withUserAndFilters(request()->filter, $user_id);
        }
        $subs_blogs = SubscriptionResource::collection($query);
        return (new PaginationService())->pagination($subs_blogs, 'subs_blogs');
    }


    /**
     * Subscribing.
     *
     * @param CreateSubscriptionRequest $request
     * @return JsonResponse
     */
    public function create(CreateSubscriptionRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $sub_in_table = Subscription::inTable($data->user_id, $data->blog_id);

        if (!empty($sub_in_table)) {
            return response()->json([
                'status' => false,
                'message' => 'You already have a subscription on this blog'
            ], 409);
        }

        $user_own_blog = Blog::isOwner($data->user_id, $data->blog_id);

        if (!empty($user_own_blog)) {
            return response()->json([
                'status' => false,
                'message' => 'You can\'t subscribe on this blog, because it is your blog'
            ], 409);
        }

        $new_sub = new Subscription(
            (new SubscriptionAttributes)->attributes($data));
        $new_sub->save();

        return response()->json([
            'response:' =>
                ['status' => true,
                    'message' => 'You are successfully subscribed'],
            'created_subscription:' =>
                new SubscriptionResource($new_sub)
        ], 201);
    }

    /**
     * Unsubscribing by id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_sub  = Subscription::query()->find($id);
        if ($current_sub !== null) {
            Subscription::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'You are unsubscribed'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Subscription doesn\'t exist'
            ], 404);
        }
    }

}
