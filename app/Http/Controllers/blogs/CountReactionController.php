<?php

namespace App\Http\Controllers\blogs;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReactionCommentWithCountsResource;
use App\Models\Reaction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CountReactionController extends Controller
{
    /**
     * Get all blogs.
     *
     * @return JsonResponse
     */
    public function show_post_stats(string $id)
    {
        $post_stats = [
            'post_id' => $id,
            'count_likes' => Reaction::likesPost($id)->count(),
            'count_dislikes' => Reaction::dislikesPost($id)->count(),
        ];
        return response()->json($post_stats);
    }


    /**
     * Get all blogs.
     *
     * @return JsonResponse
     */
    public function show_comment_stats(string $id): JsonResponse
    {
        $comment_stats = [
            'comment_id' => $id,
            'count_likes' => Reaction::likesComment($id)->count(),
            'count_dislikes' => Reaction::dislikesComment($id)->count(),
        ];
        return response()->json($comment_stats);
    }

}
