<?php

namespace App\Http\Controllers\blogs;

use App\Http\Attributes\ReactionAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateReactionRequest;
use App\Http\Resources\ReactionResource;
use App\Models\Reaction;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class ReactionController extends Controller
{
    /**
     * Get all reactions.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (!empty(request()->filter) and !empty(request()->reaction))
        {
            $query = Reaction::withFilterAndReaction(request()->filter, request()->reaction);
        }
        elseif (!empty(request()->filter))
        {
            $query = Reaction::withFilter(request()->filter);
        }
        elseif (!empty(request()->reaction))
        {
            $query = Reaction::withReaction(request()->reaction);
        }  else {
            $query = Reaction::all();
        }
        $reactions = ReactionResource::collection($query);
        return (new PaginationService())->pagination($reactions, 'reactions');
    }


    /**
     * Get reaction by id.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id)
    {
        $query = Reaction::query()->find($id);
        if (empty($query)) {
            return response()->json([
                'status' => false,
                'massage' => 'Reaction doesn\'t exist'
            ], 404);
        }
        $reaction = new ReactionResource($query);
        return response()->json(
            $reaction);
    }


    /**
     * Create a new reaction.
     *
     * @param CreateReactionRequest $request
     * @return JsonResponse
     */
    public function create(CreateReactionRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        if (Reaction::query()->where(
            (new ReactionAttributes)->attributes($data)
            )->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Reaction already exist'
            ], 409);
        }

        $new_reaction = new Reaction(
            (new ReactionAttributes)->attributes($data));
        $new_reaction->save();

        return response()->json([
            'response:' =>
                ['status' => true,
                    'message' => 'Reaction was successfully created'],
            'created_reaction:' =>
                new ReactionResource($new_reaction)
        ], 201);


    }



    /**
     * Delete a reaction by id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id)
    {
        $current_reaction  = Reaction::query()->find($id);
        if ($current_reaction !== null) {
            Reaction::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Reaction was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Reaction doesn\'t exist'
            ], 404);
        }
    }
}
