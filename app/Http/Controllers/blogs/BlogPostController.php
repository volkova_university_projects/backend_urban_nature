<?php

namespace App\Http\Controllers\blogs;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BlogPostController extends Controller
{
    /**
     * Get all blogs.
     *
     * @return JsonResponse
     */
    public function show_blog_posts(): JsonResponse
    {
        if (empty(request()->filter))
        {
            $query = Post::byBlog(request()->id);
        } else {
            $query = Post::byBlogWithFilter(request()->id, request()->filter);
        }
        $posts_in_blog = PostResource::collection($query);
        return (new PaginationService())->pagination($posts_in_blog, 'posts_in_blog');
    }
}
