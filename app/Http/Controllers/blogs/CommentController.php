<?php

namespace App\Http\Controllers\blogs;

use App\Http\Attributes\CommentAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Get all comments.
     *
     * @return JsonResponse
     */
    public function show_all()
    {
        if (empty(request()->filter)) {
            $query = Comment::all();
        } else {
            $query = Comment::withFilters(request()->filter);
        }
        $comments = CommentResource::collection($query);
        return (new PaginationService())->pagination($comments, 'comments');
    }


    /**
     * Get comment by id.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id)
    {
        $query = Comment::query()->find($id);
        if (empty($query)) {
            return response()->json([
                'status' => false,
                'massage' => 'Sorry, comment doesn\'t exist'
            ], 404);
        }
        $comment= new CommentResource($query);
        return response()->json(
            $comment);
    }


    /**
     * Create a new comment.
     *
     * @param CreateCommentRequest $request
     * @return JsonResponse
     */
    public function create(CreateCommentRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $new_comment = new Comment(
            (new CommentAttributes)->attributes($data));
        $new_comment->save();

        return response()->json([
            'response:' =>
                ['status' => true,
                    'message' => 'Comment was successfully created'],
            'created_comment:' =>
                new CommentResource($new_comment)
        ], 201);

    }


    /**
     * Edit a comment by id.
     *
     * @param UpdateCommentRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateCommentRequest $request, string $id): JsonResponse
    {
        $current_comment = Comment::query()
            ->where('id', $id)->first();

        if (empty($current_comment)) {
            return response()->json([
                'status' => false,
                'message' => 'Comment not found'
            ], 404);
        }

        $comment = request()->only(
            "comment_text",
        );

        $validator = Validator::make($comment, [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        foreach (array_keys($comment) as $key)
        {
            if (!empty($comment[$key]) && $current_comment->$key != $comment[$key]) {
                $current_comment->$key = $comment[$key];
            }
        }

        if ($current_comment->isDirty()) {
            $current_comment->save();
            return response()->json([
                'response:' =>
                    ['status' => true,
                        'message' => 'Comment was successfully updated'],
                'updated_comment:' =>
                    new CommentResource($current_comment)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'No changes'
            ]);
        }
    }


    /**
     * Delete a comment by id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_comment  = Comment::query()->find($id);
        if ($current_comment !== null) {
            Comment::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Comment was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Comment doesn\'t exist'
            ], 404);
        }
    }
}
