<?php

namespace App\Http\Controllers\blogs;

use App\Http\Attributes\PostAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Get all posts.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Post::all();
        } else {
            $query = Post::withFilters(request()->filter);
        }
        $posts = PostResource::collection($query);
        return (new PaginationService())->pagination($posts, 'posts');
    }


    /**
     * Get post by id.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id)
    {
        $query = Post::query()->find($id);
        if (empty($query)) {
            return response()->json([
                'status' => false,
                'massage' => 'Post doesn\'t exist'
            ], 404);
        }
        $post = new PostResource($query);
        return response()->json(
            $post);
    }


    /**
     * Create a new post.
     *
     * @param CreatePostRequest $request
     * @return JsonResponse
     */
    public function create(CreatePostRequest $request)
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $new_post = new Post(
            (new PostAttributes)->attributes($data));
        $new_post->save();

        return response()->json([
            'response:' =>
                ['status' => true,
                    'message' => 'Post was successfully created'],
            'created_post:' =>
                new PostResource($new_post)
        ], 201);

    }


    /**
     * Edit a post by id.
     *
     * @param UpdatePostRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdatePostRequest $request, string $id): JsonResponse
    {
        $current_post = Post::query()
            ->where('id', $id)->first();

        if (empty($current_post)) {
            return response()->json([
                'status' => false,
                'message' => 'Post not found'
            ], 404);
        }

        $post = request()->only(
            "post_title",
            "post_text",
            "photos"
        );

        $validator = Validator::make($post, [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        foreach (array_keys($post) as $key)
        {
            if (!empty($post[$key]) && $current_post->$key != $post[$key]) {
                $current_post->$key = $post[$key];
            }
        }

        if ($current_post->isDirty()) {
            $current_post->save();
            return response()->json([
                'response:' =>
                    ['status' => true,
                        'message' => 'Post was successfully updated'],
                'updated_post:' =>
                    new PostResource($current_post)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'No changes'
            ]);
        }
    }


    /**
     * Delete a post by id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id)
    {
        $current_post = Post::query()->find($id);
        if ($current_post !== null) {
            Post::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Post was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Post doesn\'t exist'
            ], 404);
        }
    }
}
