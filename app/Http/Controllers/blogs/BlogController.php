<?php

namespace App\Http\Controllers\blogs;

use App\Http\Attributes\BlogAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use App\Http\Resources\BlogResource;
use App\Models\Blog;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{

    /**
     * Get all blogs.
     *
     * @return JsonResponse
     */
    public function show_all()
    {
        if (empty(request()->filter)) {
            $query = Blog::all();
        } else {
            $query = Blog::withFilters(request()->filter);
        }
        $blogs = BlogResource::collection($query);
        return (new PaginationService())->pagination($blogs, 'blogs');
    }



    /**
     * Get all blogs for owner.
     *
     * @return JsonResponse
     */
    public function show_for_owner(string $id)
    {
        if (empty(request()->filter)) {
            $query = Blog::owner($id);
        } else {
            $query = Blog::ownerWithFilters($id, request()->filter);
        }
        $blogs_for_owner = BlogResource::collection($query);
        return (new PaginationService())->pagination($blogs_for_owner, 'blogs_for_owner');
    }


    /**
     * Get blog by id.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id)
    {
        $query = Blog::query()->find($id);
        if (empty($query)) {
            return response()->json([
                'status' => false,
                'massage' => 'Sorry, blog doesn\'t exist'
            ], 404);
        }
        $blog = new BlogResource($query);
        return response()->json(
            $blog);
    }


    /**
     * Create a new blog.
     *
     * @param CreateBlogRequest $request
     * @return JsonResponse
     */
    public function create(CreateBlogRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $new_blog = new Blog(
            (new BlogAttributes)->attributes($data));
        $new_blog->save();

        return response()->json([
            'response:' =>
                ['status' => true,
                    'message' => 'Blog was successfully created'],
            'created_blog:' =>
                new BlogResource($new_blog)
        ], 201);


    }


    /**
     * Edit a blog by id.
     *
     * @param UpdateBlogRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateBlogRequest $request, string $id): JsonResponse
    {
        $current_blog = Blog::query()
            ->where('id', $id)->first();

        if (empty($current_blog)) {
            return response()->json([
                'status' => false,
                'message' => 'Blog not found'
            ], 404);
        }

        $blog = request()->only(
            "user_id",
            "blog_title",
            "blog_description",
            "blog_avatar"
        );

        $validator = Validator::make($blog, [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        foreach (array_keys($blog) as $key)
        {
            if (!empty($blog[$key]) && $current_blog->$key != $blog[$key]) {
                $current_blog->$key = $blog[$key];
            }
        }

        if ($current_blog->isDirty()) {
            $current_blog->save();
            return response()->json([
                'response:' =>
                    ['status' => true,
                        'message' => 'Blog was successfully updated'],
                'updated_blog:' =>
                    new BlogResource($current_blog)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'No changes'
            ]);
        }
    }


    /**
     * Delete a blog by id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_blog  = Blog::query()->find($id);
        if ($current_blog !== null) {
            Blog::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Blog was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Blog doesn\'t exist'
            ], 404);
        }
    }
}
