<?php

namespace App\Http\Controllers\blogs;

use App\Http\Attributes\CategoryAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Get all categories.
     *
     * @return JsonResponse
     */
    public function show_all()
    {
        if (empty(request()->filter)) {
            $query = Category::all();
        } else {
            $query = Category::withFilters(request()->filter);
        }
        $categories = CategoryResource::collection($query);
        return (new PaginationService())->pagination($categories, 'categories');
    }

    /**
     * Create a new category.
     *
     * @param CreateCategoryRequest $request
     * @return JsonResponse
     */
    public function create(CreateCategoryRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $category_in_table = Category::query()
            ->where('category_name', 'ilike', $data->category_name)
            ->first();

        if (!empty($category_in_table)) {
            return response()->json([
                'status' => false,
                'message' => 'Category -> ' . $data->category_name . ' -> already exists'
            ], 409);
        }

        $new_category = new Category(
            (new CategoryAttributes)->attributes($data));
        $new_category->save();

        return response()->json([
            'response:' =>
                ['status' => true,
                    'message' => 'Category was successfully created'],
            'created_category:' =>
                new CategoryResource($new_category)
        ], 201);
    }


    /**
     * Edit a category by id.
     *
     * @param UpdateCategoryRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateCategoryRequest $request, string $id): JsonResponse
    {
        $current_category = Category::query()
            ->where('id', $id)->first();

        if (empty($current_category)) {
            return response()->json([
                'status' => false,
                'message' => 'Category not found'
            ], 404);
        }

        $category = request()->only(
            "category_name"
        );

        $validator = Validator::make($category, [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $category_in_table = Category::query()
            ->where('category_name', 'ilike', request()->category_name)
            ->first();

        if (!empty($category_in_table) and
            request()->category_name != $current_category->category_name) {
            return response()->json([
                'status' => false,
                'message' => 'Category -> ' . request()->category_name . ' -> already exists'
            ], 409);
        }

        foreach (array_keys($category) as $key)
        {
            if (!empty($category[$key]) && $current_category->$key != $category[$key]) {
                $current_category->$key = $category[$key];
            }
        }

        if ($current_category->isDirty()) {
            $current_category->save();
            return response()->json([
                'response:' =>
                    ['status' => true,
                        'message' => 'Category was successfully updated'],
                'updated_category:' =>
                    new CategoryResource($current_category)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'No changes'
            ]);
        }
    }


    /**
     * Delete a category by id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_category  = Category::query()->find($id);
        if ($current_category !== null) {
            Category::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Category was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Category doesn\'t exist'
            ], 404);
        }
    }

}
