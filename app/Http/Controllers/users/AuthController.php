<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Attributes\RegisterAttributes;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * Register in the system
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            $new_user = new User(
                (new RegisterAttributes())->attributes($data)
            );
            $new_user->save();

            $client_role = Role::query()
                ->where('role', '=','client')
                ->first();
            $registered_user = User::query()
                ->where('id', $new_user->id)
                ->first();

            $registered_user->roles()->attach($client_role);

            $credentials = $request->only('email', 'password');
            $token = Auth::attempt($credentials);
            // TODO: зочем??
            $user = Auth::user();
            $refresh_token = auth()->setTTL(300)->attempt($credentials);
            return response()->json([
                'user_id' => $new_user->id,
                'status' => true,
                'message' => 'Successfully registered',
                'token' => $token,
                'refresh_token' => $refresh_token,
            ], 200);
        }
    }

    /**
     * Login in the system
     *
     * @param LoginRequest $request
     * @return Response|JsonResponse
     */
    public function login(LoginRequest $request): Response|JsonResponse
    {
        $data = request();
        $validate_user = Validator::make($data->all(), [$request]);
//        error_log($data);

        if($validate_user->fails()){
            return response()->json([
                'status' => false,
                'message' => $validate_user->errors()
            ], 401);
        }

        $credentials = $data->only('email', 'password');

        $token = Auth::attempt($credentials);
        if (!$token) {
            return response()->json([
                'message' => 'Login failed',
            ], 400);
        }
        // возвращаем токен JWT
        $user = Auth::user();
        $refresh_token = auth()->setTTL(300)->attempt($credentials);
        return response()->json([
            'token' => $token,
            'refresh_token' => $refresh_token,
        ], 200);
    }

    /**
     * Logout from the system
     *
     * @return Response
     */
    public function logout(): Response
    {
        Auth::logout();
        return response()->noContent();
    }


    /**
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return response()->json([
            'token' => auth()->tokenById(auth()->user()->id),
            'refresh_token' => Auth::setTTL(300)->refresh(),
        ]);
    }


}
