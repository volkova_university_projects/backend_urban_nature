<?php

namespace App\Http\Controllers\users;

use App\Http\Attributes\RegisterAttributes;
use App\Http\Attributes\UserAttributes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\ProfileResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserWithFilterResource;
use App\Models\Role;
use App\Models\User;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {

        if (empty(request()->filter)) {
            $query = User::all();
        } else {
            $query = User::usersWithFilter(request()->filter);
        }
        $users = UserWithFilterResource::collection($query);
        return (new PaginationService())->pagination($users, 'users');
    }



    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $user = User::query()->where('id', $id)->first();
        if (empty($user)) {
            return response()->json([
                'status' => false,
                'massage' => 'User doesn\'t exist'
            ], 404);
        } else {
            return response()->json([
                new UserResource($user)
            ], 200);
        }
    }



    /**
     * Register in the system
     *
     * @param CreateUserRequest $request
     * @return JsonResponse
     */
    public function create(CreateUserRequest $request): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            $new_user = new User(
                (new UserAttributes())->attributes($data)
            );
            $new_user->save();

            $client_role = Role::query()
                ->where('role', '=','client')
                ->first();
            $created_user = User::query()
                ->where('id', $new_user->id)
                ->first();

            $created_user->roles()->attach($client_role);

            return response()->json([
                'response:' =>
                    ['status' => true,
                        'message' => 'User was successfully created with default client role'],
                'created_user:' =>
                    new UserResource($created_user)
            ]);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request, string $id): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        $current_user = User::query()
            ->where('id', $id)->first();

        if (empty($current_user)) {
            return response()->json([
                'status' => false,
                'massage' => 'User doesn\'t exist'
            ], 404);
        }

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->first_name)) {
                $current_user->first_name = $data->first_name;
            }
            if (!empty($data->last_name)) {
                $current_user->last_name = $data->last_name;
            }
//            if (!empty($data->full_name)) {
//                $current_user->full_name = $data->full_name;
//            }
            if (!empty($data->birth_date)) {
                $current_user->birth_date = $data->birth_date;
            }
            if (!empty($data->city)) {
                $current_user->city = $data->city;
            }
            if (!empty($data->phone)) {
                if ($data->phone != $current_user->phone
                    and User::query()->where('phone', $data->phone)->exists()) {
                    return response()->json([
                        'status' => false,
                        'message' => 'This phone already exist'
                    ], 409);
                } else {
                    $current_user->phone = $data->phone;
                }
            }
            if (!empty($data->email)) {
                if ($data->email != $current_user->email
                    and User::query()
                        ->where('email', $data->email)->exists()) {
                    return response()->json([
                        'status' => false,
                        'message' => 'This email already exist'
                    ], 409);
                } else {
                    $current_user->email = $data->email;
                }
            }
            if (!empty($data->password)) {
                $current_user->password = Hash::make($data->password);
            }

            if ($current_user->isDirty()) {
                $current_user->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'User was successfully updated'],
                    'updated_user:' =>
                        new UserResource($current_user)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        if (empty(User::query()->find($id)->id)) {
            return response()->json([
                'status' => false,
                'message' => 'This user doesn\'t exist'
            ], 404);
        } else {
            User::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'User was successfully deleted'
            ], 200);
        }
    }
}
