<?php

namespace App\Http\Controllers\rating_system;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserActivityController extends Controller
{
    //    Route::get('/activity/posts/{id}', 'show_activity_post');
    //    Route::get('/activity/threads/{id}', 'show_activity_thread');
    //    Route::get('/activity/blogs/{id}', 'show_activity_blog');


    /**
     * Get total count of system points for user.
     *
     * @return JsonResponse
     */
    public function show_activity_blog(string $blog_id): JsonResponse
    {
        $count_posts = Post::countItems($blog_id);
        $posts = Post::query()->where('blog_id', '=', $blog_id)->get();

//        error_log($blog_id);
//        error_log($posts);
        $count_comments = [];

        foreach ($posts as $post)
        {
            $count_comments[] = Comment::query()->where('post_id', '=', $post->id)->get()->count();
        }
        return response()->json(array_sum($count_comments));

//        $likes = Reaction::userLikes($id)->count()/100_000;
//        $dislikes = -Reaction::userDislikes($id)->count()/100_000;
//        $sum = $likes + $dislikes; // we need add count of event and special bonuses
//        $total = number_format((string)$sum, 4, '.', '');
//        $user_rating = [
//            'user_id' => $id,
//            'total_count' => floatval($total)
//        ];
//        error_log(implode(' ', $user_rating));
        return response()->json($count_posts);
    }

}
