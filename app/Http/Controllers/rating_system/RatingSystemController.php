<?php

namespace App\Http\Controllers\rating_system;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Reaction;
use App\Models\Subscription;
use App\Models\Thread;
use App\Models\User;
use App\Services\PaginationService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class RatingSystemController extends Controller
{
    /**
     * Get total count of system points for user.
     *
     * @return JsonResponse
     */
    public function show_user_rating(string $id): JsonResponse
    {
        $likes = Reaction::userLikes($id)->count()/100_000;
        $dislikes = -Reaction::userDislikes($id)->count()/100_000;
        $count_comments = Comment::byUser($id)->count()/100_000;
        $count_user_subs = Subscription::withUser($id)->count()/1_000;
        $count_own_blogs = Blog::owner($id)->count()/100;

        $sum = $likes + $dislikes + $count_comments + $count_user_subs + $count_own_blogs; // we need add count of event and special bonuses
        $total = number_format((string)$sum, 4, '.', '');
        $user_rating = [
            'user_id' => $id,
            'total_count' => floatval($total)
        ];
        return response()->json($user_rating);
    }


    /**
     * Get all user's ratings ordered by desc
     *
     * @return JsonResponse
     */
    public function show_all_users_rating(): JsonResponse
    {
        if (!empty(request()->filter)) {
            $users = User::usersWithFilter(request()->filter);
        } else {
            $users = User::all();
        }

        $users_ratings = $users->filter(function ($user)
        {
            $likes = Reaction::userLikes($user->id)->count() / 100_000;
            $dislikes = -Reaction::userDislikes($user->id)->count() / 100_000;
            $count_comments = Comment::byUser($user->id)->count()/100_000;
            $count_user_subs = Subscription::withUser($user->id)->count()/1_000;
            $count_own_blogs = Blog::owner($user->id)->count()/100;

            $sum = $likes + $dislikes + $count_comments + $count_user_subs + $count_own_blogs;
            $total = number_format((string) $sum, 4, '.', '');
            return $total != 0;
        }
        )->map(function ($user)
        {
            $likes = Reaction::userLikes($user->id)->count() / 100_000;
            $dislikes = -Reaction::userDislikes($user->id)->count() / 100_000;
            $count_comments = Comment::byUser($user->id)->count()/100_000;
            $count_user_subs = Subscription::withUser($user->id)->count()/1_000;
            $count_own_blogs = Blog::owner($user->id)->count()/100;

            $sum = $likes + $dislikes + $count_comments + $count_user_subs + $count_own_blogs;
            $total = number_format((string) $sum, 4, '.', '');
            return [
                'user_id' => $user->id,
                'user_first_name' => $user->first_name,
                'user_last_name' => $user->last_name,
                'total_count' => floatval($total)
            ];
        }
        )->sortByDesc('total_count')
            ->values();
        return (new PaginationService())->pagination($users_ratings, 'users_ratings');
    }




    /**
     * Get total count of system points for blog.
     *
     * @return JsonResponse
     */
    public function show_blog_rating(string $id): JsonResponse
    {
        $likes = Reaction::blogLikes($id)->count()/100_000;
        $dislikes = -Reaction::blogDislikes($id)->count()/100_000;
        $count_posts = Post::countItems($id)/1_000;
        $count_subs = Subscription::countItems($id)/1_000;

        $posts = Post::byBlog($id); // for counting comments by post
        $comments = [];

        foreach ($posts as $post) {
            $comments[] = Comment::byPost($post->id);
        }

        $sum_comments = array_sum($comments)/1_000;

        $sum = $likes + $dislikes + $count_posts + $sum_comments + $count_subs;
        $total = number_format((string)$sum, 4, '.', '');

        $blog_rating = [
            'blog_id' => $id,
            'total_count' => floatval($total)
        ];
        return response()->json($blog_rating);
    }

    /**
     * Get all blogs's ratings ordered by desc
     *
     * @return JsonResponse
     */
    public function show_all_blogs_rating(): JsonResponse
    {
        if (!empty(request()->filter)) {
            $blogs = Blog::withFilters(request()->filter);
        } else {
            $blogs = Blog::all();
        }

        $blogs_ratings = $blogs->filter(function ($blog)
        {
            $likes = Reaction::blogLikes($blog->id)->count() / 100_000;
            $dislikes = -Reaction::blogDislikes($blog->id)->count() / 100_000;
            $count_posts = Post::countItems($blog->id)/1_000;
            $count_subs = Subscription::countItems($blog->id)/1_000;
            $posts = Post::byBlog($blog->id); // for counting comments by post

            $comments = [];
            foreach ($posts as $post) {
                $comments[] = Comment::byPost($post->id);
            }

            $sum_comments = array_sum($comments)/1_000;
            $sum = $likes + $dislikes + $count_posts + $sum_comments + $count_subs;
            $total = number_format((string) $sum, 4, '.', '');
            return $total != 0;
        }
        )->map(function ($blog)
        {
            $likes = Reaction::blogLikes($blog->id)->count() / 100_000;
            $dislikes = -Reaction::blogDislikes($blog->id)->count() / 100_000;
            $count_posts = Post::countItems($blog->id)/1_000;
            $count_subs = Subscription::countItems($blog->id)/1_000;
            $posts = Post::byBlog($blog->id); // for counting comments by post

            $comments = [];
            foreach ($posts as $post) {
                $comments[] = Comment::byPost($post->id);
            }

            $sum_comments = array_sum($comments)/1_000;
            $sum = $likes + $dislikes + $count_posts + $sum_comments + $count_subs;
            $total = number_format((string) $sum, 4, '.', '');
            return [
                'blog_id' => $blog->id,
                'user_id' => $blog->user->id,
                'user_first_name' => $blog->user->first_name,
                'user_last_name' => $blog->user->last_name,
                'blog_title' => $blog->blog_title,
                'blog_description' => $blog->blog_description,
                'blog_avatar' => $blog->blog_avatar,
                'total_count' => floatval($total)
            ];
        }
        )->sortByDesc('total_count')
            ->values();
        return (new PaginationService())->pagination($blogs_ratings, 'blogs_ratings');
    }


    /**
     * Get total count of system points for blog.
     *
     * @return JsonResponse
     */
    public function show_comment_rating(string $id): JsonResponse
    {
        $likes = Reaction::commentLikes($id)->count()/ 10_000;
        $dislikes = -Reaction::commentDislikes($id)->count()/ 10_000;
        $sum = $likes + $dislikes;
        $total = number_format((string)$sum, 4, '.', '');
        $comment_rating = [
            'comment_id' => $id,
            'total_count' => floatval($total)
        ];
        return response()->json($comment_rating);
    }



    /**
     * Get all comment's ratings ordered by desc
     *
     * @return JsonResponse
     */
    public function show_all_comments_rating(): JsonResponse
    {
        if (!empty(request()->filter)) {
            $comments = Comment::withFilters(request()->filter);
        } else {
            $comments = Comment::all();
        }

        $comments_ratings = $comments->filter(function ($comment)
        {
            $likes = Reaction::commentLikes($comment->id)->count() / 10_000;
            $dislikes = -Reaction::commentDislikes($comment->id)->count() / 10_000;
            $sum = $likes + $dislikes;
            $total = number_format((string) $sum, 4, '.', '');
            return $total;
        }
        )->map(function ($comment)
        {
            $likes = Reaction::commentLikes($comment->id)->count() / 10_000;
            $dislikes = -Reaction::commentDislikes($comment->id)->count() / 10_000;
            $sum = $likes + $dislikes;
            $total = number_format((string) $sum, 4, '.', '');
            return [
                'comment_id' => $comment->id,
                'user_id' => $comment->user->id,
                'user_first_name' => $comment->user->first_name,
                'user_last_name' => $comment->user->last_name,
                'post_id' => $comment->post_id,
                'chat_id' => $comment->chat_id,
                'comment_text' => $comment->comment_text,
                'created' => $comment->created_at,
                'edited' => $comment->updated_at,
                'total_count' => floatval($total)
            ];
        }
        )->sortByDesc('total_count')->sortBy('created_at')
            ->values();
        return (new PaginationService())->pagination($comments_ratings, 'comments_ratings');
    }


    /**
     * Get total count of system points for blog.
     *
     * @return JsonResponse
     */
    public function show_post_rating(string $id): JsonResponse
    {
        $likes = Reaction::postLikes($id)->count()/10_000;
        $dislikes = -(Reaction::postDislikes($id)->count())/10_000;
        $count_comments = Comment::byPost($id)/1_000;
        $sum = $likes + $dislikes + $count_comments;
        $total = number_format((string)$sum, 4, '.', '');

        $post_rating = [
            'post_id' => $id,
            'total_count' => floatval($total)
        ];
        return response()->json($post_rating);
    }


    /**
     * Get total count of system points for blog.
     *
     * @return JsonResponse
     */
    public function show_all_posts_rating(): JsonResponse
    {
        if (!empty(request()->filter)) {
            $posts = Post::withFilters(request()->filter);
        } else {
            $posts = Post::all();
        }

        $posts_ratings = $posts->filter(function ($post)
//        $posts_ratings = Post::all()->filter(function ($post)
        {
            $likes = Reaction::postLikes($post->id)->count() / 10_000;
            $dislikes = -Reaction::postDislikes($post->id)->count() / 10_000;
            $count_comments = Comment::byPost($post->id)/1_000;

            $sum = $likes + $dislikes + $count_comments;
            $total = number_format((string) $sum, 4, '.', '');
            return $total;
        }
        )->map(function ($post)
        {
            $likes = Reaction::postLikes($post->id)->count() / 10_000;
            $dislikes = -Reaction::postDislikes($post->id)->count() / 10_000;
            $count_comments = Comment::byPost($post->id)/1_000;

            $sum = $likes + $dislikes + $count_comments;
            $total = number_format((string) $sum, 4, '.', '');
            return [
                'post_id' => $post->id,
                'blog_id' => $post->blog_id,
                'blog_title' => $post->blog->blog_title,
                'user_id' => $post->blog->user->id,
                'user_first_name' => $post->blog->user->first_name,
                'user_last_name' => $post->blog->user->last_name,
                'post_title' => $post->post_title,
                'post_text' => $post->post_text,
                'categories' => Category::withPost($post->id),
                'photos' => $post->photos,
                'created' => $post->created_at,
                'edited' => $post->updated_at,
                'total_count' => floatval($total)
            ];
        }
        )->sortByDesc('total_count')->sortBy('created_at')
            ->values();
        return (new PaginationService())->pagination($posts_ratings, 'posts_ratings');
    }


    /**
     * Get total count of system points for thread.
     *
     * @return JsonResponse
     */
    public function show_thread_rating(string $id): JsonResponse
    {
        $likes = Reaction::threadLikes($id)->count()/10_000;
        $dislikes = -(Reaction::threadDislikes($id)->count())/10_000;
        $count_comments = Comment::byThread($id)/1_000;
        $sum = $likes + $dislikes + $count_comments;
        $total = number_format((string)$sum, 4, '.', '');

        $thread_rating = [
            'thread_id' => $id,
            'total_count' => floatval($total)
        ];
        return response()->json($thread_rating);
    }


    /**
     * Get total count of system points for thread.
     *
     * @return JsonResponse
     */
    public function show_all_threads_rating(): JsonResponse
    {
        if (!empty(request()->filter)) {
            $thread = Thread::withFilters(request()->filter);
        } else {
            $thread = Thread::all();
        }

        $threads_ratings = $thread->filter(function ($thread)
        {
            $likes = Reaction::threadLikes($thread->id)->count()/10_000;
            $dislikes = -(Reaction::threadDislikes($thread->id)->count())/10_000;
            $count_comments = Comment::byThread($thread->id)/1_000;

            $sum = $likes + $dislikes + $count_comments;
            $total = number_format((string) $sum, 4, '.', '');
            return $total;
        }
        )->map(function ($thread)
        {
            $likes = Reaction::threadLikes($thread->id)->count()/10_000;
            $dislikes = -(Reaction::threadDislikes($thread->id)->count())/10_000;
            $count_comments = Comment::byThread($thread->id)/1_000;

            $sum = $likes + $dislikes + $count_comments;
            $total = number_format((string) $sum, 4, '.', '');
            return [
                'thread_id' => $thread->id,
                'user_id' => $thread->user->id,
                'user_first_name' => $thread->user->first_name,
                'user_last_name' => $thread->user->last_name,
                'thread_title' => $thread->thread_title,
                'created' => $thread->created_at,
                'edited' => $thread->updated_at,
                'total_count' => floatval($total)
            ];
        }
        )->sortByDesc('total_count')->sortBy('created_at')
            ->values();
        return (new PaginationService())->pagination($threads_ratings, 'threads_ratings');
    }
}
