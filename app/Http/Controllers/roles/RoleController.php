<?php

namespace App\Http\Controllers\roles;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Models\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        $query = Role::all();
        $roles = RoleResource::collection($query);
        return response()->json([
            'roles' => $roles
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Role::query()->find($id);
        $role = new RoleResource($query);
        return response()->json(
            $role);
    }
}
