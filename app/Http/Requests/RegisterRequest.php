<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class RegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'birth_date' => 'required|date_format:Y-m-d',
            'city' => 'nullable|string',
            'phone' => 'required|regex:/^[+]?[0-9]{1,3}\([0-9]{3}\)[0-9]{3}[.-][0-9]{2}[.-][0-9]{2}$/i',
            'email' => 'required|email|unique:App\Models\User,email',
            'password' => 'required|string'
        ];
    }
}
