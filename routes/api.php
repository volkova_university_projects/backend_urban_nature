<?php


use App\Http\Controllers\blogs\BlogPostController;
use App\Http\Controllers\blogs\CategoryController;
use App\Http\Controllers\blogs\SubscriptionController;
use App\Http\Controllers\blogs\ThreadController;
use App\Http\Controllers\categories\CityController;
use App\Http\Controllers\blogs\CountReactionController;
use App\Http\Controllers\events\CompetitionEventController;
use App\Http\Controllers\events\EventController;
use App\Http\Controllers\rating_system\UserActivityController;
use App\Http\Controllers\rating_system\RatingSystemController;
use App\Http\Controllers\roles\RoleController;
use App\Http\Controllers\roles\RoleUserController;
use App\Http\Controllers\users\AuthController;
use App\Http\Controllers\users\ProfileController;
use App\Http\Controllers\users\UserController;
use App\Http\Controllers\blogs\PostController;
use App\Http\Controllers\blogs\CommentController;
use App\Http\Controllers\blogs\BlogController;
use App\Http\Controllers\blogs\ReactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

// users

Route::controller(AuthController::class)->group(function () {
    Route::post('/account/register', 'register');
    Route::post('/account/login', 'login');
    Route::post('/account/logout', 'logout');
    Route::get('/account/refresh', 'refresh');
});


Route::controller(ProfileController::class)->group(function () {
    Route::get('/account/profile', 'show');
    Route::put('/account/profile', 'update');
});

Route::controller(UserController::class)->group(function () {
    Route::get('/users', 'show_all');
    Route::post('/users', 'create'); // creation with default role. for adding another roles you must call patch method for roles with user_id
    Route::get('/users/{id}', 'show');
    Route::put('/users/{id}', 'update');
    Route::delete('/users/{id}', 'delete');
});

Route::controller(RoleController::class)->group(function () {
    Route::get('/roles', 'show_all');
    Route::get('/roles/{id}', 'show'); //get role by role's id
});

Route::controller(RoleUserController::class)->group(function () {
    Route::patch('/roles/{user_id}', 'update'); // create new roles for user
    Route::delete('/roles/{user_id}', 'delete'); // delete existing roles for user
});

Route::controller(CityController::class)->group(function () {
    Route::get('/cities', 'show_all');
//    Route::get('/cities/{id}', 'show');
    Route::post('/cities', 'create');
    Route::put('/cities/{id}', 'update');
    Route::delete('/cities/{id}', 'delete');
});


// blogs

Route::controller(BlogController::class)->group(function () {
    Route::get('/blogs', 'show_all');
    Route::post('/blogs', 'create');
    Route::get('/blogs/{id}', 'show');
    Route::get('/blogs/users/{id}', 'show_for_owner');
    Route::put('/blogs/{id}', 'update');
    Route::delete('/blogs/{id}', 'delete');
});

Route::controller(PostController::class)->group(function() {
   Route::get('/posts', 'show_all');
   Route::post('/posts', 'create');
   Route::get('/posts/{id}', 'show');
   Route::put('/posts/{id}', 'update');
   Route::delete('/posts/{id}', 'delete');
});

Route::controller(CommentController::class)->group(function () {
   Route::get('/comments', 'show_all');
   Route::post('/comments', 'create');
   Route::get('/comments/{id}', 'show');
   Route::put('/comments/{id}', 'update');
   Route::delete('/comments/{id}', 'delete');
});

Route::controller(ReactionController::class)->group(function () {
   Route::get('/reactions', 'show_all'); // for admin maybe
   Route::post('/reactions', 'create');
   Route::get('/reactions/{id}', 'show');
   Route::delete('/reactions/{id}', 'delete');
});

Route::controller(CountReactionController::class)->group(function () {
    Route::get('/posts/{id}/stats', 'show_post_stats');
    Route::get('/comments/{id}/stats', 'show_comment_stats');
});

Route::controller(BlogPostController::class)->group(function () {
    Route::get('/blogs/{id}/posts', 'show_blog_posts');
});

Route::controller(RatingSystemController::class)->group(function () {
   Route::get('ratings/users/{id}', 'show_user_rating');
   Route::get('ratings/blogs/{id}', 'show_blog_rating');
   Route::get('ratings/comments/{id}', 'show_comment_rating');
   Route::get('ratings/posts/{id}', 'show_post_rating');
   Route::get('ratings/threads/{id}', 'show_thread_rating');
   Route::get('/ratings/events/{id}', 'show_event_rating');
   Route::get('ratings/users', 'show_all_users_rating');
   Route::get('ratings/posts', 'show_all_posts_rating');
   Route::get('ratings/comments', 'show_all_comments_rating');
   Route::get('ratings/blogs', 'show_all_blogs_rating');
   Route::get('ratings/threads', 'show_all_threads_rating');
   Route::get('ratings/events', 'show_all_events_ratings');
});


Route::controller(ThreadController::class)->group(function () {
    Route::get('/threads', 'show_all');
    Route::post('/threads', 'create');
    Route::put('/threads/{id}', 'update');
    Route::delete('/threads/{id}', 'delete');
});

Route::controller(SubscriptionController::class)->group(function () {
    Route::get('/subscriptions/{user_id}', 'show_sub_blogs');
    Route::post('/subscriptions', 'create');
    Route::delete('/subscriptions/{id}', 'delete');
});

Route::controller(CategoryController::class)->group(function () {
   Route::get('/categories', 'show_all');
   Route::post('/categories', 'create');
   Route::put('/categories/{id}', 'update');
   Route::delete('/categories/{id}', 'delete');
});


//Actions and events

Route::controller(EventController::class)->group(function () {
   Route::get('/events', 'show_all');
   Route::post('/events', 'create') ;
   Route::put('/events/{id}', 'update');
   Route::delete('/events/{id}', 'delete');
});

Route::controller(CompetitionEventController::class)->group(function () {
   Route::get('/co-events', 'show_all');
   Route::post('/co-events', 'create');
   Route::put('/co-events', 'update');
   Route::delete('/co-events', 'delete');
});


